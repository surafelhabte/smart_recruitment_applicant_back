<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Validate_Token {
    
    public function __construct(){
        $CI =& get_instance();
        $CI->load->helper(['jwt', 'authorization']);
        $CI->load->helper('url');
    }

    public function authenticateToken($header){
        if($header) {
            $token = $header['Authorization'];
            try {
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    return false;
                } else {
                    return true;
                }
            } catch (Exception $e) {
                return $e;
            }
        }
    }

    public function generateToken($data){
        try {
            $token = AUTHORIZATION::generateToken($data);
            return $token;
        } catch (Exception $e) {
            return $e;
        } 
    }
}