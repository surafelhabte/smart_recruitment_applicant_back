<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UploadFiles {
    
    public function __construct(){}

    public function uploadFile($parameter)
    {
        $field;
        $config;
        $data = array();

        $CI =& get_instance();

        $field = $parameter['field'];
        $config['upload_path']  = $parameter['uploadlocation'];
        $config['allowed_types'] = $parameter['allowedExtenstion'];
        $config['max_size']  = $parameter['allowedSize'];
        if($parameter['allowedExtenstion'] == 'jpeg|png|jpg'){
            $config['file_name'] = str_replace(".","-",$parameter['fileName']). '.jpeg';
        } else {
            $config['file_name'] = $parameter['fileName'];
        }
        $config['overwrite'] = TRUE;
        $config['file_ext_tolower'] = TRUE;

        $data=[];

        if (!is_dir($parameter['uploadlocation'])) {
            mkdir($parameter['uploadlocation'], 0777, true);
        }

        $CI->load->library('upload');
        $CI->upload->initialize($config);
        if (!$CI->upload->do_upload($parameter['field']))
        {
            $error = $CI->upload->display_errors();
            return $error;
        }
        else
        {
            $data[$parameter['field']] = $CI->upload->data();
        }
        return $data;
    }

    public function deleteFile($path)
    {
        if (unlink($path)) {
            return true;
        } else {
            return false;
        }
    }
}