<?php 
   Class User_Model extends CI_Model { 
    
        Public function __construct() { 
            parent::__construct(); 
        } 
      
        public function login($data) {
            $query = "Email='" . $data['email'] . "' AND Password='" . md5($data['password']) . "' AND Activated='True'";
            $result = $this->db->get_where('Applicant', $query)->result_array();
            return $result;
        }

        public function signup($data) {           
            $link = $data['link'];
            unset($data['link']);
            
            $email_aleady_exist_result = count($this->check_email($data['Email']));
            if($email_aleady_exist_result < 1){
                $this->db->trans_begin();

                $data['Password'] = md5($data['Password']);
                if (!empty($_FILES)){
                    $data = $this->upload_photo($data);
                    if(count($data['data']) > 0){
                        $this->db->set($data['data']);
                        $this->db->insert('Applicant');   
                    } else {
                        return ['status'=>false, 'message' => $data['error']];
                    } 
                } else {
                    $this->db->set($data);
                    $this->db->insert('Applicant'); 
                }
                if($this->db->trans_status() === true){
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'Registerd Successfully.'];
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'Registration failed.'];
                }

                $this->load->helper('email');
                if(Verification_email($data['FirstName'], $data['Email'], $link ,'Varifay Email')) {
                    return ['status'=>true, 'message' =>'Registerd Successfully. verification message is sent.'];  
                } else {
                    return ['status'=>true, 'message' =>'Registerd Successfully. But unable to send verification email.'];
                }
            } else {
                return ['status'=>false, 'message' =>'Email is already in use.'];
            }
        }
    
        public function check_email($Email,$id = NULL) {
            $id === NULL ? $query = "Email = '" . $Email ."'" : $query = "Email = '" . $Email ."' AND id!='" . $id ."'";
            $result = $this->db->get_where('Applicant', $query)->result_array();
            return $result;
        }
    
        public function getProfile($id) {
            $result = $this->db->get_where('Applicant', array('id' => $id))->result_array();
            return $result;
        }

        public function updateProfile($data) {
            $uploadResult = array();
            $delete_success = false;
            $email_aleady_exist_result = count($this->check_email($data['Email'],$data['id']));
            if($email_aleady_exist_result < 1){
                $this->db->trans_begin();
                if (!empty($_FILES)){
                    $data = $this->upload_photo($data);
                    if(count($data['data']) > 0 ){
                     $this->db->update('Applicant', $data['data'], array('id' => $data['data']['id']));   
                    } else {
                       return ['status'=>false, 'message' => $data['error']]; 
                    }
                } else {
                    $this->db->update('Applicant', $data, array('id' => $data['id']));
                } 
                if($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'profile updated successfully.'];
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'profile not updated']; 
                } 
            } else {
                return ['status'=>false, 'message' =>'Email is aleady in use. please try another.'];
            }
        }

        public function changePassword($data){
            $old_Password_correct = false;
            $this->db->select('Password');
            $result = $this->db->get_where('Applicant', array('id' => $data['id']))->row();
            if($result->Password == md5($data['OldPassword'])) {
                $old_Password_correct = true;
                unset($data['OldPassword']);
                $data['Password'] = md5($data['Password']);
            }
            if($old_Password_correct){
                $this->db->trans_begin();
                if($this->db->update('Applicant', $data, array('id' => $data['id'])))
                {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'Password Changed successfully.'];
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to change password.'];
                }
            } else {
                return ['status'=>false, 'message' =>'Old Password is not correct.'];
            }
        }

        public function isActivated($Email) {
            $result = $this->db->get_where('Applicant', array('Email' => $Email))->row();
            if($result->Activated === 'False'){
                return ['status'=>false, 'message' =>'Account is not activated.'];
            }
        }

        public function setNewPassword($data) {
            $this->db->trans_begin();
            if($this->db->update('Applicant', $data, array('id' => $data['id']))){
                $this->db->trans_commit();
                return ['status'=>true, 'message' =>'New password set successfully.'];
            } else {
                $this->db->trans_rollback();
                return ['status'=>false, 'message' =>'unable to set New password.'];
            }
        }

        public function findEmail($data) {
            $link = $data['Link'];
            unset($data['Link']);
            $this->db->select('Email,FirstName,id');
            $result = $this->db->get_where('Applicant', array('Email' => $data['Email']))->row();
            if(!is_null($result)){
                $this->load->helper('email');
                if(email($result->FirstName, $result->Email, $link . $this->validate_token->generateToken($result->id) ,'Passsword reset link from ECX Recruitment System')) {
                    return ['status'=>true, 'message' => 'password reset link sent successfully. please check your inbox.'];  
                } else {
                    return ['status'=>false, 'message' => 'unable to send password reset link. please check your network.'];
                }
            } else {
                return ['status' => false, 'message' => 'Sorry the email you enter is invalid ! please try again.'];
            }
        }

        public function sendPasswordResetEmail($data) {
            $this->load->helper('email');
            if(email($data['FirstName'], $data['email'], $data['link'] ,'Passsword reset link')) {
                return ['status'=>true, 'message' =>'password reset link sent successfully. please check your inbox.'];  
            } else {
                return ['status'=>false, 'message' =>'unable to send password reset link. please check your network.'];
            }
        }

        private function upload_photo($data){
            $params = array('field' => 'photo', 'uploadlocation' => 'uploads/Applicants/',
             'allowedExtenstion' => 'jpeg|png|jpg','allowedSize' => '1e+7', 'fileName' =>  $data['Email']);
            $this->load->library('UploadFiles');
            $uploadResult = $this->uploadfiles->UploadFile($params);

            if(is_array($uploadResult) && count($uploadResult) > 0)
            {
                foreach($uploadResult as $upload)
                {
                    $data['Photo'] = base_url('uploads/Applicants/' . $upload['file_name']);
                }
                return array('data' => $data,'error' => []);
            } else {
                return array('data' => [],'error' => strip_tags($uploadResult));
            }
        }
   }   
?>