<?php 
   Class Lookup_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
    
    public function load_lookups($post) {
      $total = array();
      foreach($post as $field){
        $this->db->select('Value As text, Value AS value');
        $result = $this->db->get_where('Lookups',array('Title' => $field))->result_array();
        $total[$field] = $result;
      }
      return $total;
    }
  } 