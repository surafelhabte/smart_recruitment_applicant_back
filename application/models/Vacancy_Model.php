<?php
   class Vacancy_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
       }
      
       public function GetVacancies()
       {
           $select = 'Vacancy.id,Vacancy.Title, Location, Availability,Term_Of_Employement,Dead_line As deadline, created_date, Remark,Jobs.Details';
           $this->db->select($select);
           $this->db->from('Vacancy');
           $this->db->where(array('dead_line >=' => date('Y-m-d')));
           $this->db->join('Jobs', 'Jobs.id = Vacancy.JobId');
           $result = $this->db->get()->result_array();
           return $result;
       }

       public function GetVacancy($id)
       {
           $select = 'Vacancy.id,Vacancy.Title, Location, Availability,Term_Of_Employement,Dead_line As deadline,created_date,
         Jobs.Special_Work_Experience, Jobs.Special_Year_Of_Experience, 
         Jobs.id As JobId, Remark,Jobs.Details, Jobs.Field_Of_Study, Jobs.Work_Experience, Jobs.Sector';

           $this->db->select($select);
           $this->db->from('Vacancy');
           $this->db->where(array('dead_line >=' => date('Y-m-d'), 'Vacancy.id' => $id));
           $this->db->join('Jobs', 'Jobs.id = Vacancy.JobId');
           $result = $this->db->get()->result_array();

           $result[0]['Field_Of_Study'] = json_decode($result[0]['Field_Of_Study'], true);
           $result[0]['Work_Experience'] = json_decode($result[0]['Work_Experience'], true);
           $result[0]['Sector'] = json_decode($result[0]['Sector'], true);

           $qualifications = $this->db->get_where('Qualification_And_Experience', array('JobId' => $result[0]['JobId']))->result_array();

           return ['vacancy' => (Object)$result[0], 'qualifications' =>$qualifications];
       }

       public function Search($keyword)
       {
           $select = 'Vacancy.id,Vacancy.Title, Location, Availability,Vacancy_Type,Term_Of_Employement,Dead_line As deadline,
         created_date,JobGrade,Department,Division,Remark';
           $this->db->select($select);
           $this->db->from('Vacancy');
           $this->db->where('MATCH (Vacancy.Title) AGAINST ("' . $keyword.'") AND dead_line >= "' . date('Y-m-d').'"');
           $this->db->join('Jobs', 'Jobs.id = Vacancy.JobId');
           $result = $this->db->get()->result_array();

           if (count($result) > 0) {
               return ['status'=>true, 'message' => $result];
           } else {
               return ['status'=>false, 'message' =>'No Record Found.'];
           }
       }

       public function Filter($data)
       {
           $query = '';
           if (!is_null($data['Location']) && !is_null($data['keyword'])) {
               $query = 'Location="'.$data['Location'].'" AND MATCH (Vacancy.Title) AGAINST ("' . $data['keyword'].'")';
               $query = $query . ' AND dead_line >= "' . date('Y-m-d') . '"';
           }
           if (empty($data['Location']) && !is_null($data['keyword'])) {
               $query = 'MATCH (Vacancy.Title) AGAINST ("' . $data['keyword'].'")';
               $query = $query . ' AND dead_line >= "' . date('Y-m-d') . '"';
           }
           if (empty($data['keyword']) && !is_null($data['Location'])) {
               $query = 'Location="'.$data['Location'].'"';
               $query = $query . ' AND dead_line >= "' . date('Y-m-d') . '"';
           }
           if (empty($data['Location']) && empty($data['keyword'])) {
               $query = null;
           }
         
           if ($query != null) {
               $select = 'Vacancy.id, Vacancy.Title, Location, Availability,Term_Of_Employement,Dead_line As deadline, created_date,Remark,Jobs.id As JobId';
               $this->db->distinct();
               $this->db->select($select);
               $this->db->from('Vacancy');
               $this->db->where($query);
               $this->db->join('Jobs', 'Jobs.id = Vacancy.JobId');
            
               if (is_null($data['Experience'])) {
                   $this->db->join('Qualification_And_Experience', 'Qualification_And_Experience.JobId = Jobs.id');
               } else {
                   $this->db->join('Qualification_And_Experience', 'Qualification_And_Experience.JobId = Jobs.id AND Qualification_And_Experience.Experience <= "' . $data['Experience'] . '"');
               }

               $result = $this->db->get()->result_array();
               count($result) > 0 ? $response =  ['status'=>true, 'message' => $result] : $response = ['status'=>false, 'message' =>'No Record Found.'];
               return $response;
           } else {
               return ['status'=>false, 'message' =>'No Record Found'];
           }
       }
   }
