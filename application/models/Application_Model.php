<?php 
   Class Application_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function Apply($data) {
      $result = $this->db->get_where('Application', array('Applicant_Id'=>$data['applicant_id'], 'VacancyId'=>$data['VacancyId']))->result_array();
      if(count($result) > 0){
        return ['status'=>false, 'message' =>['You Already aaplied for this Vacancy.']];
      } else {
        $validation = $this->validate_Qualification($data);
        if($validation['status']){
          $this->db->trans_begin();
          $this->db->set($data);
          if($this->db->insert('Application'))
          {
            $this->db->trans_commit();
            return ['status'=>true, 'message' =>['Your application sent successfully.']];
          } else {
            $this->db->trans_rollback();
            return ['status'=>false, 'message' =>'unable to send application, please try again later.'];
          }       
        } else {
          return ['status'=>false, 'message' =>$validation['message']];
        }
     }
    }

    public function GetApplications($data) {
        $this->db->select('Vacancy.Title,Vacancy.id As Vacancy_Id,Application.Status, Application.id, Application.created_date As Application_Date');
        $this->db->from('Application');
        $this->db->where('Applicant_Id', $data['applicant_id']);
        $this->db->join('Vacancy', 'Application.VacancyId = Vacancy.id');
        $result = $this->db->get()->result_array();
        
        foreach($result as &$value){
          switch($value['Status']){
            case 'InProcess':
              $res = $this->db->select('AssesementFor,Date')->order_by('Date', 'DESC')->from('AssessmentSchedule')->where(array('VacancyId' => $value['Vacancy_Id']))->get()->row(0);
              $value['next'] = $res == null ? null: $res->AssesementFor;
              break;
            case 'Pass':
              $value['next'] = 'Assessment Completed !';
              break;
            case 'Failed':
              $value['next'] = 'Not Available !';
              break;
          }
          unset($value['Vacancy_Id']);
        }

      return $result;  
    }

    public function GetApplication($data) {
      $result = $this->db->get_where('Application', array('id' => $data['id']))->row();
      return $result;
    }

    public function Withdraw($data) {
      $this->db->trans_begin();
      if($this->db->delete('Application', array('id' => $data['id'])))
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'You withdraw from this candidacy successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to withdraw, please try again.'];
      }
    }
 
    private function validate_Qualification($data){ 
      $errors = array();
      $this->db->select("Qualification,Field_Of_Study");
      $app_qualification = $this->db->get_where('Qualifications', array('CVId' => $data['CVId']))->result_array();

      $this->db->select("SUM(DATEDIFF(`EmployedTo`, `EmployedFrom`)) As Experience");
      $app_experience = $this->db->get_where('Experiences', array('CVId' => $data['CVId']))->row();
      $app_experience = (Object)['Experience'=> ceil($app_experience->Experience / 365)];

      $this->db->select('JobId');
      $JobId = $this->db->get_where('Vacancy', array('id' => $data['VacancyId']))->row();

      $this->db->select("Field_Of_Study");
      $Field_Of_Study = $this->db->get_where('Jobs', array('id' => $JobId->JobId))->row();
      $Field_Of_Study = json_decode($Field_Of_Study->Field_Of_Study, true);

      $this->db->select('Qualification,Experience');
      $Job_QualificationAndExperience = $this->db->get_where('Qualification_And_Experience', array('JobId' => $JobId->JobId))->result_array();

      foreach($Job_QualificationAndExperience as $field => $value1){
        foreach($app_qualification as $field => $value2){
          if($value1['Qualification'] == $value2['Qualification']){
            if(in_array($value2['Field_Of_Study'], $Field_Of_Study)){
              if($value1['Experience'] <= $app_experience->Experience){
                $message = [];
                $message = ['status' => true, 'message' => 'Qualified'];
                return $message;
              } else {
                array_push($errors, 'Your Work Experience Is Not Enough For : ' . $value1['Experience']);
                $message = ['status' => false, 'message' => $errors];
              }
            } else {
              array_push($errors, 'Your Field Study Is Not Enough For : ' . implode( ", ", $Field_Of_Study));
              $message = ['status' => false, 'message' => $errors];
            }
          } else {
            array_push($errors, 'Your Qualification Not Enough For : ' . $value1['Qualification']);
            $message = ['status' => false, 'message' => $errors];
          }
        }
      }
      return $message;
    }

   } 
?>