<?php 
   Class CV_Model extends CI_Model { 
      Public function __construct() { 
         parent::__construct(); 
      } 
      
    public function Create_Cv($data) {
      $BatchArray = array();
      $setError = 0;
      $Document_Uploaded = false;
      $cv = json_decode($data['cv'], true);

      $this->db->trans_begin();

      if (!empty($_FILES)){
        $uploadResult = $this->uploadDocument($cv);
        if(is_array($uploadResult) && count($uploadResult) > 0){
          $cv['DocumentName'] = $uploadResult['Document']['file_name'];
          $cv['Document'] = 'uploads/Documents/' . $cv['Applicant_Id'] . '/' . $cv['Title']. '/'. $uploadResult['Document']['file_name'];
          $Document_Uploaded = true;
        } 
      }

      $this->db->set($cv);
      if($this->db->insert('CVs'))
      {
        $id = $this->db->insert_id();
        $BatchArray['Qualifications'] = count(json_decode($data['Qualifications'], true)) > 0 ? $this->convertToArray($data['Qualifications'],$id) : [];
        $BatchArray['References'] = count(json_decode($data['References'], true)) > 0 ? $this->convertToArray($data['References'],$id) : [];
        $BatchArray['Trainings'] = count(json_decode($data['Trainings'], true)) > 0 ? $this->convertToArray($data['Trainings'],$id): [];
        $BatchArray['Experiences'] = count(json_decode($data['Experiences'], true)) > 0 ? $this->convertToArray($data['Experiences'],$id):[];
        
        foreach($BatchArray as $field => $value) {
          if(count($value) > 0){
            if(!$this->db->insert_batch($field, $value['data_with_id']))
            {
              $setError = $setError + 1;
              break;
            }
          }
        }
        if($setError == 0){
          $this->db->trans_commit();
          if(!$Document_Uploaded) {
           return ['status'=>false,'message'=>'CV created Successfully. but Document not Uploaded'];
          } 
          return ['status'=>true,'message'=>'CV Created Successfully !'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false,'message'=>'CV Not Created'];
        }
      } else {
        $this->db->trans_rollback();
        return ['status'=>false,'message'=>'CV Not Created'];
      } 
    }

    public function Update_Cv($data) {
      $status=false;
      $BatchArray = array();
      $Document_Uploaded = false;
      $cv = json_decode($data['cv'], true);

      if (!empty($_FILES)){
        $uploadResult = $this->uploadDocument($cv);
        if(is_array($uploadResult) && count($uploadResult) > 0){
          $cv['Document'] = 'uploads/Documents/' . $cv['Applicant_Id'] . '/'  . $cv['Title']. '/'. $uploadResult['Document']['file_name'];
          $cv['DocumentName'] = $uploadResult['Document']['file_name'];
          $Document_Uploaded = true;
        } 
      }
      
      $this->db->trans_begin();
      $this->db->where('id', $cv['id']);
      $this->db->set($cv);
     
      if($this->db->update('CVs'))
      {
        $BatchArray['Qualifications'] = count(json_decode($data['Qualifications'], true)) > 0 ? $this->convertToArray($data['Qualifications'],$cv['id'],true) : [];
        $BatchArray['References'] = count(json_decode($data['References'], true)) > 0 ? $this->convertToArray($data['References'],$cv['id'],true) : [];
        $BatchArray['Trainings'] = count(json_decode($data['Trainings'], true)) > 0 ? $this->convertToArray($data['Trainings'],$cv['id'],true): [];
        $BatchArray['Experiences'] = count(json_decode($data['Experiences'], true)) > 0 ? $this->convertToArray($data['Experiences'],$cv['id'],true):[];

        foreach($BatchArray as $field => $value) {
          if(count($value) > 0){
            if(count($value['data_with_id']) > 0){
              $this->db->update_batch($field,$value['data_with_id'],'id');
            }
            if(count($value['data_without_id']) > 0){
              $this->db->insert_batch($field, $value['data_without_id']);
            }

            if ($this->db->trans_status() === FALSE)
            {            
              break;
            }
          }
        }

        if($this->db->trans_status() === TRUE){
          $this->db->trans_commit();
          $message=isset($Document_Uploaded) && (!$Document_Uploaded)?'Cv Updated successfully, but document not uploaded':'Cv Updated';
          $status=true;
        } else {
          $this->db->trans_rollback();
          $message='Cv Not Updated';
        }
      } 
      return ['status'=>$status,'message'=>$message];
    }
  
    public function Get_Cvs($post) {
      $this->db->select('id,Title,DocumentName');
      $result = $this->db->get_where('CVs', array('Applicant_Id' => $post['applicant_id']))->result_array();
      return $result;
    }

    public function Get_Cv($id) {
      $cv = array();
      $result = $this->db->get_where('CVs',array('id' => $id))->row();

      $cv['cv'] = $result;
      $cv['Qualifications'] = $this->db->get_where('Qualifications',array('CVId' => $result->id))->result_array();
      $cv['References'] = $this->db->get_where('References',array('CVId' => $result->id))->result_array();
      $cv['Trainings'] = $this->db->get_where('Trainings',array('CVId' => $result->id))->result_array();
      $cv['Experiences'] = $this->db->get_where('Experiences',array('CVId' => $result->id))->result_array();

      return $cv;
    }

    public function Delete_Cv($data) {
      $this->db->trans_begin();
      if($this->deleteDocument($data)){
        if($this->db->delete('CVs', array('id' => $data['id'])))
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'CV Deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to Delete CV.'];
        }
      } else {
        return 3;
      } 
    }        
     
    public function Delete_Item($data) {
      $this->db->trans_begin();
      if($this->db->delete($data['table'], array('id' => $data['id'])))
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'Item Deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to Delete Item.'];
      }
    } 

    private function uploadDocument($data) { 
      if (!empty($_FILES)){
          $params = array('field' => 'Document', 
          'uploadlocation' => 'uploads/Documents/' . $data['Applicant_Id'] . '/' . $data['Title'], 
          'fileName' => $data['Applicant_Id'], 
          'allowedExtenstion' => 'pdf|docx|doc','allowedSize' => '1e+7');
          $this->load->library('UploadFiles');
          $uploadResult = $this->uploadfiles->UploadFile($params);
          return $uploadResult;
      } else {
        return [];
      }
    }
   
    private function deleteDocument($data) { 
      $delete_path = 'uploads/Documents/' . $data['applicant_id'] . '/' . $data['Title'] . '/' . $data['DocumentName'];
      $this->load->library('UploadFiles');
      if($this->uploadfiles->deleteFile($delete_path)){
        return true;  
      } else {
        return false;
      } 
    }

    public function convertToArray($data, $id, $forUpdate = false) {
      $data_without_id = array();
      $Converted_Data = json_decode($data, true);

      foreach($Converted_Data as $key => &$c_data) {
        $c_data['CVId'] = $id;
        if($forUpdate){
          if(empty($c_data['id'])){
            array_push($data_without_id, $c_data);
            array_splice($Converted_Data,$key,1);
          }
        }
      }
      return ['data_with_id' => $Converted_Data, 'data_without_id' => $data_without_id];
    }

  } 