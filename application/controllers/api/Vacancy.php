<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Vacancy extends CI_Controller {

    use REST_Controller {
    REST_Controller::__construct as private __resTraitConstruct;
  }
 
    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
    }
 
    public function GetVacancies_get() {
        $result = $this->Vacancy_Model->GetVacancies();
        $this->response($result, REST::HTTP_OK);
    }

    public function GetVacancy_get($id)
    {
        $result = $this->Vacancy_Model->GetVacancy($id);
        $this->response($result, REST::HTTP_OK);
    }

    public function Search_get($keyword)
    {
        $result = $this->Vacancy_Model->Search($keyword);
        $this->response($result, REST::HTTP_OK);
    }

    public function Filter_post()
    {
        $post = $this->post();
        $result = $this->Vacancy_Model->Filter($post);
        $this->response($result, REST::HTTP_OK);
    }
}
