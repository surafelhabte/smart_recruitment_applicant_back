<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class User extends CI_Controller {

    use REST_Controller {
    REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
    }
 
    public function login_post() {
        $post = $this->post();
        $result = $this->User_Model->login($post);
        if(count($result) > 0) {
            $result[0]['key'] = $this->validate_token->generateToken($post['email']);
        }
        $this->response($result, REST::HTTP_OK);
    }

    public function signup_post()
    {
        $post = $this->post();
        $post['link'] = AUTHORIZATION::generateToken($post['Email']);
        $result = $this->User_Model->signup($post);
        $this->response($result, REST::HTTP_OK);
    }

    public function getProfile_get($id) {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->User_Model->getProfile($id);
            if($result) {
              unset($result[0]['Password']);
              $this->response($result[0], REST::HTTP_OK);
            }
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function updateProfile_post() {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->User_Model->updateProfile($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function changePassword_post(){
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->User_Model->changePassword($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function setNewPassword_post() {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $post['id'] = AUTHORIZATION::validateToken($post['id']);
            $post['Password'] = md5($post['Password']);
            $result = $this->User_Model->setNewPassword($post);
            $this->response($result, REST::HTTP_OK);  
        } else {
            $this->response(['status' => false, 'message' => 'Key Expired. please try reset again.'], REST::HTTP_OK);
        }
    }

    public function findEmail_post() {
        $post = $this->post();
        $result = $this->User_Model->findEmail($post);
        $this->response($result, REST::HTTP_OK);
    }

    public function isActivated_get($Email) {
        $result = $this->User_Model->isActivated($Email);
        if($result) {
          $this->response($result, REST::HTTP_OK);
        }
    }

    public function sendPasswordResetEmail_post() {
        $post = $this->post();
        $post['link'] = AUTHORIZATION::generateToken($post['email']);
        $result = $this->User_Model->sendPasswordResetEmail($post);
        if($result) {
          $this->response($result, REST::HTTP_OK);
        }  
    } 
}
