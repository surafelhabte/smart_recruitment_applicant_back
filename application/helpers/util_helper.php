<?php

function pre($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}
function exits($array)
{
    pre($array);
    exit();
}

function json($array)
{
    header("Content-Type: application/json; charset=UTF-8");

    echo json_encode($array);
}